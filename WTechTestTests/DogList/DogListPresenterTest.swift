//
//  DogListPresenterTest.swift
//  WTechTestTests
//
//  Created by Jae Kwang Lee on 2020/02/19.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import XCTest
@testable import WTechTest

class DogListPresenterTest: XCTestCase {
    
    override func setUp() {
    }
    
    override func tearDown() {
    }
    
    // MARK: - Module Test
    func testViewDidLoadShouldCallViewUpdate() {
        let mockInteractor = DogListInteractorMock()
        let mockRouter = DogListRouterMock()
        let presenter = DogListPresenter(interactor: mockInteractor, router: mockRouter)
        let mockView = DogListViewMock()
        mockView.presenter = presenter
        presenter.view = mockView
        mockInteractor.presenter = presenter
        
        let expectation = self.expectation(description: "viewDidLoad should call startLoading(), hideLoading() and udpateView()")

        presenter.viewDidLoad()
        XCTAssertTrue(mockView.isShowLoading)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
        
        XCTAssertTrue(mockView.isHideLoading)
        XCTAssertTrue(mockView.isUpdateView)
    }
    
    func testViewDidLoadShouldFailAndAlert() {
        let mockInteractor = DogListInteractorMock(isLoadingSuccessful: false)
        let mockRouter = DogListRouterMock()
        let presenter = DogListPresenter(interactor: mockInteractor, router: mockRouter)
        let mockView = DogListViewMock()
        mockView.presenter = presenter
        presenter.view = mockView
        mockInteractor.presenter = presenter
        
        let expectation = self.expectation(description: "viewDidLoad should call startLoading(), hideLoading() and showAlert()")

        presenter.viewDidLoad()
        XCTAssertTrue(mockView.isShowLoading)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
        
        XCTAssertTrue(mockView.isHideLoading)
        XCTAssertTrue(mockView.isShowAlert)
    }
    
    func testOnItemClickedShouldCallRouterPush() {
        let mockInteractor = DogListInteractorMock()
        let mockRouter = DogListRouterMock()
        let presenter = DogListPresenter(interactor: mockInteractor, router: mockRouter)
        let mockView = DogListViewMock()
        mockView.presenter = presenter
        presenter.view = mockView
        mockInteractor.presenter = presenter
        
        presenter.onItemClicked(id: "dummyId")
        
        XCTAssertTrue(mockRouter.isPushToDogDetail)
    }
    
}
