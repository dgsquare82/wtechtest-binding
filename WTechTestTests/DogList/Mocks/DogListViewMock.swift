//
//  DogListViewMock.swift
//  WTechTestTests
//
//  Created by Jae Kwang Lee on 2020/02/19.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit
@testable import WTechTest

class DogListViewMock: DogListViewController {
    
    var isUpdateView: Bool = false
    var isShowLoading: Bool = false
    var isHideLoading: Bool = false
    var isShowAlert: Bool = false
    var updateViewSortText: String = ""
        
    override func updateView(cellModels: [DogListViewCellModel], sortText: String) {
        self.isUpdateView = true
        self.updateViewSortText = sortText
    }
    
    override func showLoading() {
        self.isShowLoading = true
    }
    
    override func hideLoading() {
        self.isHideLoading = true
    }
    
    override func showAlert(titie: String, message: String) {
        self.isShowAlert = true
    }
}
