//
//  DogListPresenterMock.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/17.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
@testable import WTechTest

class DogListPresenterMock: DogListPresenterProtocol {
    var interactor: DogListInteractorProtocol
    var view: DogListViewProtocol? = nil
        
    var isViewDidLoad: Bool = false
    var isOnListLoaded: Bool = false
    var isOnListLoadFalied: Bool = false
    var isUpdateView: Bool = false
    var isOnItemClicked: Bool = false
    var isOnSortClicked: Bool = false
    
    init(interactor: DogListInteractorProtocol) {
        self.interactor = interactor
    }
    
    func viewDidLoad() {
        self.isViewDidLoad = true
    }

    func onListLoaded(dogs: [DogItem]) {
        self.isOnListLoaded = true
    }
    
    func onListLoadFalied() {
        self.isOnListLoadFalied = true
    }
    
    func updateView() {
        self.isUpdateView = true
    }
    
    func onItemClicked(id: String) {
        self.isOnItemClicked = true
    }
    
    func onSortClicked() {
        self.isOnSortClicked = true
    }

}
