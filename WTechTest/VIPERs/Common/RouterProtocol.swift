//
//  RouterProtocol.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit

protocol RouterProtocol: class {
    static func createModule() -> UIViewController
}
