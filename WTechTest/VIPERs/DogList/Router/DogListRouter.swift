//
//  DogListRouter.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit

class DogListRouter: DogListRouterProtocol {
    func pushToDogDetail(id: String, view: UIViewController) {
        let targetView = DogDetailRouter.createModule()
        view.navigationController?.pushViewController(targetView, animated: true)
    }
        
    static func createModule() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "DogListViewController") as! DogListViewController
        
        let provider = DogListProviderREST()
        let interactor = DogListInteractor(provider: provider)
        let router = DogListRouter()
        view.presenter = DogListPresenter(interactor: interactor, router: router)
        view.presenter.view = view
        interactor.presenter = view.presenter
        
        return view
    }
}
