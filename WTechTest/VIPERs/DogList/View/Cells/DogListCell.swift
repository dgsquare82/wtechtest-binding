//
//  DogListCell.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import UIKit

class DogListCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var lifespanFormatted: UILabel!
    @IBOutlet weak var photoImageView: UIImageView! {
        didSet {
            self.photoImageView.layer.cornerRadius = photoImageView.bounds.height / 2.0
        }
    }

    var cellModel: DogListViewCellModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func updateView(model: DogListViewCellModel) {
        self.cellModel = model

        self.nameLabel.text = cellModel.name
        self.descLabel.text = cellModel.desc
        self.lifespanFormatted.text = cellModel.lifespanFormatted
        self.photoImageView.image = UIImage(named: "place-holder")

        ImageDownloadManager.shared.downloadImage(url: cellModel.photoURL, isCircular: true) { [weak self] response in
            guard let strongSelf = self else { return }
            
            switch (response) {
            case .imageResponse(let image, let url):
                // This is to prevent the 'Race Condition' issue.
                if url.absoluteString == strongSelf.cellModel.photoURL {
                    strongSelf.photoImageView.image = image
                }
            case .error(_):
                ()
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
