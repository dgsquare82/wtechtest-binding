//
//  ViewController.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/17.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import UIKit
import MBProgressHUD

class DogListViewController: UIViewController, DogListViewProtocol {
    var cellModels = [DogListViewCellModel]()
    var hud: MBProgressHUD? = nil

    var presenter: DogListPresenterProtocol!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Dog List"
        
        self.tableView.register(UINib(nibName: DogListCell.className, bundle: nil), forCellReuseIdentifier: DogListCell.className)
        
        self.presenter.viewDidLoad()
    }
    
    func showAlert(titie: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateView(cellModels: [DogListViewCellModel], sortText: String) {
        self.cellModels = cellModels
        self.tableView.reloadData()
        self.sortButton.title = sortText
    }
    
    func showLoading() {
        if self.hud == nil {
            self.hud = MBProgressHUD.showAdded(to: self.tableView, animated: true)
            hud?.removeFromSuperViewOnHide = true
        }
    }
    
    func hideLoading() {
        self.hud?.hide(animated: true)
        self.hud = nil
    }
    
    @IBAction func onSortClicked(_ sender: Any) {
        self.presenter.onSortClicked()
    }
}

extension DogListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cellModels.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // It's important to let the table view know the estimaed height for scrolling.
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = nil
        
        let cellModel = cellModels[indexPath.section]
        
        let dogCell = tableView.dequeueReusableCell(withIdentifier: DogListCell.className, for: indexPath) as! DogListCell
        dogCell.updateView(model: cellModel)
        cell = dogCell as UITableViewCell
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellModel = cellModels[indexPath.section]
        self.presenter.onItemClicked(id: cellModel.id)
    }
}

