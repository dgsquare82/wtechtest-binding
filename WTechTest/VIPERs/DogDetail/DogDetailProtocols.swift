//
//  DogDetailProtocols.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit

protocol DogDetailViewProtocol: class {
    // Todo
}

protocol DogDetailPresenterProtocol: class {
    // Todo
}

protocol DogDetailInteractorProtocol: class {
    // Todo
}

protocol DogDetailRouterProtocol: RouterProtocol {
    // Todo
}
