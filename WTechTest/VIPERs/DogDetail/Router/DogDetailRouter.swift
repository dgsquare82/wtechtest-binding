//
//  DogListDetailRouter.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import UIKit

class DogDetailRouter: DogDetailRouterProtocol {
    class func createModule() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "DogDetailViewController") as? DogDetailViewController
        // Todo: Setup VIPER here.
        return view!
    }
}
