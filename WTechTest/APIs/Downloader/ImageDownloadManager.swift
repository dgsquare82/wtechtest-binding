//
//  ImageDownloader.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/18.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

// ImageDownloadManager uses Alamofire and AlamofireImage for image downloads and caching.
// https://github.com/Alamofire/AlamofireImage

enum ImageDownloadResponse {
    case imageResponse(UIImage, URL)
    case error(ImageDownloadError)
}

enum ImageDownloadError: Error {
    case notValidResponse
}

class ImageDownloadManager {
    private let downloader = ImageDownloader(
        configuration: ImageDownloader.defaultURLSessionConfiguration(),
        downloadPrioritization: .fifo,
        maximumActiveDownloads: 8,
        imageCache: AutoPurgingImageCache()
    )
    
    static var shared: ImageDownloadManager {
        struct __ { static let _sharedInstance = ImageDownloadManager() }
        return __._sharedInstance
    }
    
    func downloadImage(url: String, isCircular: Bool, completion: ((ImageDownloadResponse) -> Void)?) {
        let urlRequest = URLRequest(url: URL(string: url)!)

        let filter = (isCircular) ? AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0)) : nil
        
        downloader.download(urlRequest, filter: filter) { response in
            if let image = response.result.value, let url = response.request?.url {
                completion?(ImageDownloadResponse.imageResponse(image, url))
            }
            else {
                completion?(ImageDownloadResponse.error(.notValidResponse))
            }
        }
    }
}

