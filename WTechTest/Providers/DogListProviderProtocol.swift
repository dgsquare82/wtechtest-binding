//
//  DogListProvider.swift
//  WTechTest
//
//  Created by Jae Kwang Lee on 2020/02/17.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation

enum DogListProviderResponse {
    case response([DogData])
    case error(DogListProviderError)
}

enum DogListProviderError {
    case responseIsNotValid
    case networkError
}

protocol DogListProviderProtocol {
    typealias completionClosure = ((DogListProviderResponse) -> Void)?
    
    func loadList(completion: completionClosure)
}
